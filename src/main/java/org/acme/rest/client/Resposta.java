package org.acme.rest.client;

import java.util.List;

public class Resposta {

    private List<Quote> value;

    public List<Quote> getValue() {
        return value;
    }

    public void setValue(List<Quote> value) {
        this.value = value;
    }
}
