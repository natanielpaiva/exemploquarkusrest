package org.acme.rest.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.org.apache.xpath.internal.operations.Quo;
import jdk.vm.ci.meta.Local;
import org.acme.rest.client.entity.QuoteEntity;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.List;

@Path("/quotes")
public class QuoteResource {

    @Inject
    @RestClient
    QuoteService quoteService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/data/{data}")
    @Transactional
    public List<LinkedHashMap> hello(@PathParam("data") String data) {
        Object o = quoteService.getTeste("'" + data + "'");
        List<LinkedHashMap> list = (List<LinkedHashMap>) ((LinkedHashMap) o).get("value");


        if (!list.isEmpty()) {
            list.forEach(linkedHashMap -> {
                QuoteEntity quoteEntity = new QuoteEntity();
                quoteEntity.setCotacaoCompra((Double) linkedHashMap.get("cotacaoCompra"));
                quoteEntity.setCotacaoVenda((Double) linkedHashMap.get("cotacaoVenda"));
                String dataHoraCotacao = (String) linkedHashMap.get("dataHoraCotacao");
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
                LocalDateTime dateTime = LocalDateTime.parse(dataHoraCotacao, formatter);
                quoteEntity.setDataHoraCotacao(dataHoraCotacao);
                quoteEntity.persist();
            });
        }

        return list;
    }
}